// Define a struct to represent a single home

typedef struct home
{
	int zip;
	char addr[40];
	int price;
	int area;
} home;
