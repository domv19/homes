#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
int count= 0;

char **readhomes(char *filename)
{     
    //Open the CSV file
    FILE *f = fopen(filename, "r");
    
    struct home *house[10000];
    int zip;
	char addr[40];
	int price;
	int area;
	
    //scan the file and add it to the home struct array
    while(scanf(" %i,%[^,],%i,%i", &zip, addr, &price, &area) != EOF)
    {
        // Allocate space for a struct home
         struct home *h = (struct home *)malloc(sizeof(struct home));
         
         // Copy values into the struct
         h->zip = zip;
         strcpy(h->addr, addr);
         h->price = price;
         h->area = area;
         
         //Add it to the array
         house[count] = h;
         count++;
    }
    
    return (char**)house;

}


//Function defining the number of homes in the listings file.
void printHomes(int count)
{
    printf("Homes: %i\n", count); 
}

int userzip;

//Function defining the number of zipcodes that match the users input.
void printZip(int userzip, int count)
{
    int zipcount = 0;
    for ( int i = 0; i < count; i++ )
    {
        if (userzip == house[i]->zip)
        {
            zipcount++;
        }                             
    }
    printf("Homes in zipcode %i: %i\n", &userzip, zipcount); 
}

int userprice;

//Function defining which houses fall within the price range specified by the user
void printRange(int userprice, int count)
{
    for ( int i = 0; i < count; i++ )
    {
        if ((house[i]->price < userprice + 10000 || house[i]->price > userprice + 10000) && (house[i]->price > userprice - 10000 || house[i]->price < userprice - 10000))
        {
            return 0;
        }
        else 
        {
            printf("%s $%i\n", house[i]->addr, house[i]->price);
        }
    } 
} 
   
int main(int argc, char *argv[])
{
    // TODO: Use readhomes to get the array of structs
    readhomes("listings.csv");
    
    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.
    printHomes(count);
    
    // TODO: Prompt the user to type in a zip code.
    printf("Please enter a zip code: \n");
    scanf("%i", &userzip); 
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    printZip(userzip, count);
    

    // TODO: Prompt the user to type in a price.
    printf("Please enter a price: \n");
    scanf("%i", &userprice);

    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    printRange(userprice, count);
}


